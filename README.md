# Probability Art - Blackjack

This is the final version of Probability Art for CS4644 Creative Computing 2022.

The initial game aspect is from Allan Lavell on pygame's website
https://www.pygame.org/project-Blackjack-640-.html

In order to run this, you will need to pip install the following packages:
pygame, pyautogui, and parse 

We also used the pickle import from the Python standard library

Run using blackjack_milestone3_demo.py

A tutorial is provided to understand how to play the game and use the controls.
