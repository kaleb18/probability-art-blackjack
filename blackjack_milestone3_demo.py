#!/usr/bin/python
# Blackjack Version 1.0, Copyright 2008 Allan Lavell
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import random
import os
import sys

import pygame
from pygame.locals import *
pygame.font.init()
pygame.mixer.init()

import pygame.gfxdraw
import pyautogui

import pickle

from parse import *

screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
lastY = 0
clock = pygame.time.Clock()

###### SYSTEM FUNCTIONS BEGIN #######
def imageLoad(name, card):
    """ Function for loading an image. Makes sure the game is compatible across multiple OS'es, as it
    uses the os.path.join function to get he full filename. It then tries to load the image,
    and raises an exception if it can't, so the user will know specifically what's going if the image loading
    does not work. """
    
    if card == 1:
        fullname = os.path.join("images/cards/", name)
    else: fullname = os.path.join('images', name)
    
    image = pygame.image.load(fullname)
    image = image.convert()
    
    return image, image.get_rect()
        
def soundLoad(name):
    """ Same idea as the imageLoad function. """
    
    fullName = os.path.join('sounds', name)
    sound = pygame.mixer.Sound(fullName)
    return sound

def display(font, sentence):
    """ Displays text at the bottom of the screen, informing the player of what is going on."""
    
    displayFont = pygame.font.Font.render(font, sentence, 1, (255,255,255), (0,0,0)) 
    return displayFont

def playClick():
    clickSound = soundLoad("click2.wav")
    clickSound.play()
###### SYSTEM FUNCTIONS END #######

###### MAIN GAME FUNCTION BEGINS ######
def mainGame():
    """ Function that contains all the game logic. """
    
    # List of actions taken in given round
    actionsList = []
    dealerHandBeforeReset = []
    playerHandBeforeReset = []
    playerWon = False
    win = [0]
    lose = [0]
    draw = [0]

    def gameOver():
        """ Displays a game over screen in its own little loop. It is called when it has been determined that the player's funds have
        run out. All the player can do from this screen is exit the game."""
        
        while 1:
            for event in pygame.event.get():
                if event.type == QUIT:
                    sys.exit()
                if event.type == KEYDOWN and event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()

            # Fill the screen with black
            screen.fill((0,0,0))
            
            # Render "Game Over" sentence on the screen
            oFont = pygame.font.Font(None, 50)
            displayFont = pygame.font.Font.render(oFont, "Game over! You're outta cash!", 1, (255,255,255), (0,0,0)) 
            screen.blit(displayFont, (125, 220))
            
            # Update the display
            pygame.display.flip()
            
    ######## DECK FUNCTIONS BEGIN ########
    def shuffle(deck):
        """ Shuffles the deck using an implementation of the Fisher-Yates shuffling algorithm. n is equal to the length of the
        deck - 1 (because accessing lists starts at 0 instead of 1). While n is greater than 0, a random number k between 0
        and n is generated, and the card in the deck that is represented by the offset n is swapped with the card in the deck
        represented by the offset k. n is then decreased by 1, and the loop continues. """
        
        n = len(deck) - 1
        while n > 0:
            k = random.randint(0, n)
            deck[k], deck[n] = deck[n], deck[k]
            n -= 1

        return deck        
                        
    def createDeck():
        """ Creates a default deck which contains all 52 cards and returns it. """

        deck = ['sj', 'sq', 'sk', 'sa', 'hj', 'hq', 'hk', 'ha', 'cj', 'cq', 'ck', 'ca', 'dj', 'dq', 'dk', 'da']
        values = range(2,11)
        for x in values:
            spades = "s" + str(x)
            hearts = "h" + str(x)
            clubs = "c" + str(x)
            diamonds = "d" + str(x)
            deck.append(spades)
            deck.append(hearts)
            deck.append(clubs)
            deck.append(diamonds)
        return deck

    def returnFromDead(deck, deadDeck):
        """ Appends the cards from the deadDeck to the deck that is in play. This is called when the main deck
        has been emptied. """
        
        for card in deadDeck:
            deck.append(card)
        del deadDeck[:]
        deck = shuffle(deck)

        return deck, deadDeck
        
    def deckDeal(deck, deadDeck):
        """ Shuffles the deck, takes the top 4 cards off the deck, appends them to the player's and dealer's hands, and
        returns the player's and dealer's hands. """

        deck = shuffle(deck)
        dealerHand, playerHand = [], []

        cardsToDeal = 4

        while cardsToDeal > 0:
            if len(deck) == 0:
                deck, deadDeck = returnFromDead(deck, deadDeck)

            # deal the first card to the player, second to dealer, 3rd to player, 4th to dealer, based on divisibility (it starts at 4, so it's even first)
            if cardsToDeal % 2 == 0: playerHand.append(deck[0])
            else: dealerHand.append(deck[0])
            
            del deck[0]
            cardsToDeal -= 1
            
        return deck, deadDeck, playerHand, dealerHand

    def hit(deck, deadDeck, hand):
        """ Checks to see if the deck is gone, in which case it takes the cards from
        the dead deck (cards that have been played and discarded)
        and shuffles them in. Then if the player is hitting, it gives
        a card to the player, or if the dealer is hitting, gives one to the dealer."""

        # if the deck is empty, shuffle in the dead deck
        if len(deck) == 0:
            deck, deadDeck = returnFromDead(deck, deadDeck)

        hand.append(deck[0])
        del deck[0]

        return deck, deadDeck, hand

    def getValueFromDeck(number):
        if number + 1 >= 10:
            return 10
        else:
            return number + 1

    def checkValue(hand):
        """ Checks the value of the cards in the player's or dealer's hand. """

        totalValue = 0

        for card in hand:
            value = card[1:]

            # Jacks, kings and queens are all worth 10, and ace is worth 11    
            if value == 'j' or value == 'q' or value == 'k': value = 10
            elif value == 'a': value = 11
            else: value = int(value)

            totalValue += value
            

        if totalValue > 21:
            for card in hand:
                # If the player would bust and he has an ace in his hand, the ace's value is diminished by 10    
                # In situations where there are multiple aces in the hand, this checks to see if the total value
                # would still be over 21 if the second ace wasn't changed to a value of one. If it's under 21, there's no need 
                # to change the value of the second ace, so the loop breaks. 
                if card[1] == 'a': totalValue -= 10
                if totalValue <= 21:
                    break
                else:
                    continue

        return totalValue

    def checkValueAceIsOne(hand):
        """ Checks the value of the cards in the player's or dealer's hand where the Ace is always considered 1. """

        totalValue = 0

        for card in hand:
            value = card[1:]

            # Jacks, kings and queens are all worth 10, and ace is worth 11    
            if value == 'j' or value == 'q' or value == 'k': value = 10
            elif value == 'a': value = 1
            else: value = int(value)

            totalValue += value

        return totalValue
        
    def blackJack(deck, deadDeck, playerHand, dealerHand, funds, bet, cards, cardSprite):
        """ Called when the player or the dealer is determined to have blackjack. Hands are compared to determine the outcome. """

        textFont = pygame.font.Font(None, 28)

        playerValue = checkValue(playerHand)
        dealerValue = checkValue(dealerHand)
        
        if playerValue == 21 and dealerValue == 21:
            # The opposing player ties the original blackjack getter because he also has blackjack
            # No money will be lost, and a new hand will be dealt
            displayFont = display(textFont, "Blackjack! The dealer also has blackjack, so it's a push!")
            deck, playerHand, dealerHand, deadDeck, funds, roundEnd = endRound(deck, playerHand, dealerHand, deadDeck, funds, 0, bet, cards, cardSprite)
                
        elif playerValue == 21 and dealerValue != 21:
            # Dealer loses
            displayFont = display(textFont, "Blackjack! You won $%.2f." %(bet*1.5))
            deck, playerHand, dealerHand, deadDeck, funds, roundEnd = endRound(deck, playerHand, dealerHand, deadDeck, funds, bet, 0, cards, cardSprite)
            
        elif dealerValue == 21 and playerValue != 21:
            # Player loses, money is lost, and new hand will be dealt
            deck, playerHand, dealerHand, deadDeck, funds, roundEnd = endRound(deck, playerHand, dealerHand, deadDeck, funds, 0, bet, cards, cardSprite)
            displayFont = display(textFont, "Dealer has blackjack! You lose $%.2f." %(bet))
            
        return displayFont, playerHand, dealerHand, deadDeck, funds, roundEnd

    def bust(deck, playerHand, dealerHand, deadDeck, funds, moneyGained, moneyLost, cards, cardSprite):
        """ This is only called when player busts by drawing too many cards. """
        
        font = pygame.font.Font(None, 28)
        displayFont = display(font, "You bust! You lost $%.2f." %(moneyLost))
        
        deck, playerHand, dealerHand, deadDeck, funds, roundEnd = endRound(deck, playerHand, dealerHand, deadDeck, funds, moneyGained, moneyLost, cards, cardSprite)
        
        return deck, playerHand, dealerHand, deadDeck, funds, roundEnd, displayFont

    def endRound(deck, playerHand, dealerHand, deadDeck, funds, moneyGained, moneyLost, cards, cardSprite):
        """ Called at the end of a round to determine what happens to the cards, the moneyz gained or lost,
        and such. It also shows the dealer's hand to the player, by deleting the old sprites and showing all the cards. """
    
        if len(playerHand) == 2 and "a" in playerHand[0] or "a" in playerHand[1]:
            # If the player has blackjack, pay his bet back 3:2
            moneyGained += (moneyGained/2.0)
            
        # Remove old dealer's cards
        cards.empty()
        
        dCardPos = (50, 70)
                   
        for x in dealerHand:
            card = cardSprite(x, dCardPos)
            dCardPos = (dCardPos[0] + 80, dCardPos [1])
            cards.add(card)

        # Remove the cards from the player's and dealer's hands
        #dealerHandBeforeReset = dealerHand.copy()
        #playerHandBeforeReset = playerHand.copy()
        del dealerHandBeforeReset[:]
        del playerHandBeforeReset[:]

        for card in playerHand:
            playerHandBeforeReset.append(card)
            deadDeck.append(card)
        for card in dealerHand:
            dealerHandBeforeReset.append(card)
            deadDeck.append(card)

        del playerHand[:]
        del dealerHand[:]

        funds += moneyGained
        funds -= moneyLost
        
        textFont = pygame.font.Font(None, 28)
        
        if funds <= 0:
            gameOver()  
        
        roundEnd = 1

        return deck, playerHand, dealerHand, deadDeck, funds, roundEnd 
        
    def compareHands(deck, deadDeck, playerHand, dealerHand, funds, bet, cards, cardSprite):
        """ Called at the end of a round (after the player stands), or at the beginning of a round
        if the player or dealer has blackjack. This function compares the values of the respective hands of
        the player and the dealer and determines who wins the round based on the rules of blacjack. """

        textFont = pygame.font.Font(None, 28)
        # How much money the player loses or gains, default at 0, changed depending on outcome
        moneyGained = 0
        moneyLost = 0

        dealerValue = checkValue(dealerHand)
        playerValue = checkValue(playerHand)
            
        # Dealer hits until he has 17 or over        
        while 1:
            if dealerValue < 17:
                # dealer hits when he has less than 17, and stands if he has 17 or above
                deck, deadDeck, dealerHand = hit(deck, deadDeck, dealerHand)
                dealerValue = checkValue(dealerHand)
            else:   
                # dealer stands
                break
            
        if playerValue > dealerValue and playerValue <= 21:
            # Player has beaten the dealer, and hasn't busted, therefore WINS
            playerWon = True
            moneyGained = bet
            deck, playerHand, dealerHand, deadDeck, funds, roundEnd = endRound(deck, playerHand, dealerHand, deadDeck, funds, bet, 0, cards, cardSprite)
            displayFont = display(textFont, "You won $%.2f." %(bet))
        elif playerValue == dealerValue and playerValue <= 21:
            # Tie
            playerWon = False
            deck, playerHand, dealerHand, deadDeck, funds, roundEnd = endRound(deck, playerHand, dealerHand, deadDeck, funds, 0, 0, cards, cardSprite)
            displayFont = display(textFont, "It's a push!")
        elif dealerValue > 21 and playerValue <= 21:
            # Dealer has busted and player hasn't
            playerWon = True
            deck, playerHand, dealerHand, deadDeck, funds, roundEnd = endRound(deck, playerHand, dealerHand, deadDeck, funds, bet, 0, cards, cardSprite)
            displayFont = display(textFont, "Dealer busts! You won $%.2f." %(bet))
        else:
            # Dealer wins in every other siutation taht i can think of
            playerWon = False
            deck, playerHand, dealerHand, deadDeck, funds, roundEnd = endRound(deck, playerHand, dealerHand, deadDeck, funds, 0, bet, cards, cardSprite)
            displayFont = display(textFont, "Dealer wins! You lost $%.2f." %(bet))
            
        return deck, deadDeck, roundEnd, funds, displayFont

    #Our deck functions to calculate different probabilities

    def cardsInDeck(deckCount):
        total = 0
        #print(len(deckCount))
        for x in range(0, len(deckCount)):
            total += deckCount[x]
        return total
    
    def getCardString(num):
        if (num == 0):
            return "a"
        elif (num == 1):
            return "2"
        elif (num == 2):
            return "3"
        elif (num == 3):
            return "4"
        elif (num == 4):
            return "5"
        elif (num == 5):
            return "6"
        elif (num == 6):
            return "7"
        elif (num == 7):
            return "8"
        elif (num == 8):
            return "9"
        elif (num == 9):
            return "10"

    def getIndex(card):
        value = card[1:]

        # Jacks, kings and queens are all worth 10, and ace is worth 11    
        if value == 'j' or value == 'q' or value == 'k': index = 10
        elif value == 'a': index = 1
        else: index = int(value)

        return index - 1


    def getDeckCounts(deck, dealerHand):
        deckCounts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    
        for x in range(0, len(deck)):
            deckCounts[getIndex(deck[x])] += 1
            #addDeck(getCard(deck[x]))
        deckCounts[getIndex(dealerHand[1])] += 1
        #print("Dealer shown: " + str(dealerHand[0]) + " Dealer Hidden: " + str(dealerHand[1]))
        return deckCounts

    # Returns the bustChance in decimal form
    def bustChance(deck, dealerHand, playerHand):
        #print("Started bust chance")
        if len(dealerHand) < 2: 
            #print("Hand empty return")
            return 0
        deckCounts = getDeckCounts(deck, dealerHand)
        #print("After getDeckCounts")
        bust = [0]
        if (checkValue(playerHand) <= 21):
            for x in range (0, len(deckCounts)):
                if deckCounts[x] > 0:
                    if checkValueAceIsOne(playerHand) + getValueFromDeck(x) > 21:
                        totalDeck = len(deck) + 1 #Deck size plus 1 for dealer hand
                        bust[0] += ((deckCounts[x])/(totalDeck))
        else:
            bust[0] = 1
        #print("Ended bust chance")
        return round((bust[0]*100),2)

    def winChance(deck, dealerHand, playerHand):
        if len(dealerHand) < 2:
            return (0, 1)[playerWon]
        deckCounts = getDeckCounts(deck, dealerHand)
        testDealer = []
        testDealer.append(dealerHand[0])
        
        win[0] = 0
        lose[0] = 0
        draw[0] = 0
        percentage = [1]
        if (checkValue(playerHand) <= 21):
            recursiveWin(testDealer, deckCounts, percentage, playerHand)
        else:
            win[0] = 0
            lose[0] = 1
            draw[0] = 0

        return round((win[0]*100),2)

    def recursiveWin(testDealer, numDeck, percentage, playerHand):
        tempDeal = testDealer.copy()
        tempDeck = numDeck.copy()
        for x in range (0, len(numDeck)):
            testDealer = tempDeal.copy()
            numDeck = tempDeck.copy()
            newPercentage = [0]
            if numDeck[x] > 0:
                newCard = "s" + getCardString(x)
                testDealer.append(newCard)
                numDeck[x] -= 1
                dealerTotal = checkValue(testDealer)
                playerTotal = checkValue(playerHand)
                numOfCardsInDeck = cardsInDeck(numDeck) + 1
                if  dealerTotal > 21:
                    newPercentage[0] = percentage[0] * ((numDeck[x]+1)/numOfCardsInDeck)
                    win[0] += newPercentage[0]
                elif dealerTotal < 17:
                    newPercentage[0] = percentage[0] * ((numDeck[x]+1)/numOfCardsInDeck)
                    recursiveWin(testDealer, numDeck, newPercentage, playerHand)
                elif (dealerValue > playerTotal):
                    newPercentage[0] = percentage[0] * ((numDeck[x]+1)/numOfCardsInDeck)
                    lose[0] += newPercentage[0]
                elif (dealerTotal == playerTotal):
                    newPercentage[0] = percentage[0] * ((numDeck[x]+1)/numOfCardsInDeck)
                    draw[0] += newPercentage[0]
                elif (dealerTotal < playerTotal):
                    newPercentage[0] = percentage[0] * ((numDeck[x]+1)/numOfCardsInDeck)
                    win[0] += newPercentage[0]

    def drawBorderedBox(screen, rect, border_width, borderColor, fillColor):
        if border_width > rect.width / 3 or border_width > rect.height / 3:
            return
        inner_vis = Rect((rect.x + border_width, rect.y + border_width), (rect.width - border_width * 2, rect.height - border_width * 2))
        pygame.gfxdraw.box(screen, rect, borderColor)
        pygame.gfxdraw.box(screen, inner_vis, fillColor)
        #print(inner_vis)
        #print(rect)
        return inner_vis

    ######## DECK FUNCTIONS END ########  
    
    ######## SPRITE FUNCTIONS BEGIN ##########
    class cardSprite(pygame.sprite.Sprite):
        """ Sprite that displays a specific card. """
        
        def __init__(self, card, position):
            pygame.sprite.Sprite.__init__(self)
            cardImage = card + ".png"
            self.image, self.rect = imageLoad(cardImage, 1)
            self.position = position
        def update(self):
            self.rect.center = self.position
            
    class hitButton(pygame.sprite.Sprite):
        """ Button that allows player to hit (take another card from the deck). """
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            self.image, self.rect = imageLoad("hit-grey.png", 0)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height/4 - 5) 
            #self.position[0] = self.position[0] - 10
            #self.position[1] = self.position[1] + 10
            #self.position = (735, 400)
            
        def update(self, mX, mY, deck, deadDeck, playerHand, cards, pCardPos, roundEnd, cardSprite, click):
            """ If the button is clicked and the round is NOT over, Hits the player with a new card from the deck. It then creates a sprite
            for the card and displays it. """
            
            if roundEnd == 0: self.image, self.rect = imageLoad("hit.png", 0)
            else: self.image, self.rect = imageLoad("hit-grey.png", 0)
            
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height/4 - 5)
            self.rect.center = self.position
            
            if self.rect.collidepoint(mX, mY) == 1 and click == 1:
                if roundEnd == 0: 
                    playClick()
                    deck, deadDeck, playerHand = hit(deck, deadDeck, playerHand)

                    actionsList.append("Hit")
                    currentCard = len(playerHand) - 1
                    card = cardSprite(playerHand[currentCard], pCardPos)
                    cards.add(card)
                    pCardPos = (pCardPos[0] - 80, pCardPos[1])
                
                    click = 0
                
            return deck, deadDeck, playerHand, pCardPos, click
            
    class standButton(pygame.sprite.Sprite):
        """ Button that allows the player to stand (not take any more cards). """
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            self.image, self.rect = imageLoad("stand-grey.png", 0)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height/4 - 35)
            #self.position = (735, 365)
            
        def update(self, mX, mY, deck, deadDeck, playerHand, dealerHand, cards, pCardPos, roundEnd, cardSprite, funds, bet, displayFont):
            """ If the button is clicked and the round is NOT over, let the player stand (take no more cards). """
            
            if roundEnd == 0: self.image, self.rect = imageLoad("stand.png", 0)
            else: self.image, self.rect = imageLoad("stand-grey.png", 0)
            
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height/4 - 35)
            #self.position = (735, 365)
            self.rect.center = self.position
            
            if self.rect.collidepoint(mX, mY) == 1:
                if roundEnd == 0: 
                    playClick()

                    actionsList.append("Stand")
                    deck, deadDeck, roundEnd, funds, displayFont = compareHands(deck, deadDeck, playerHand, dealerHand, funds, bet, cards, cardSprite)
                
            return deck, deadDeck, roundEnd, funds, playerHand, deadDeck, pCardPos, displayFont 
            
    class doubleButton(pygame.sprite.Sprite):
        """ Button that allows player to double (double the bet, take one more card, then stand)."""
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            self.image, self.rect = imageLoad("double-grey.png", 0)
            #self.position = (735, 330)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height/4 - 65)
            
        def update(self, mX, mY,   deck, deadDeck, playerHand, dealerHand, playerCards, cards, pCardPos, roundEnd, cardSprite, funds, bet, displayFont):
            """ If the button is clicked and the round is NOT over, let the player stand (take no more cards). """
            
            if roundEnd == 0 and funds >= bet * 2 and len(playerHand) == 2: self.image, self.rect = imageLoad("double.png", 0)
            else: self.image, self.rect = imageLoad("double-grey.png", 0)
                
            #self.position = (735, 330)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height/4 - 65)
            self.rect.center = self.position
                
            if self.rect.collidepoint(mX, mY) == 1:
                if roundEnd == 0 and funds >= bet * 2 and len(playerHand) == 2: 
                    bet = bet * 2
                    
                    playClick()
                    deck, deadDeck, playerHand = hit(deck, deadDeck, playerHand)

                    currentCard = len(playerHand) - 1
                    card = cardSprite(playerHand[currentCard], pCardPos)
                    playerCards.add(card)
                    pCardPos = (pCardPos[0] - 80, pCardPos[1])
                    actionsList.append("Double")
                    deck, deadDeck, roundEnd, funds, displayFont = compareHands(deck, deadDeck, playerHand, dealerHand, funds, bet, cards, cardSprite)
                    
                    bet = bet / 2

            return deck, deadDeck, roundEnd, funds, playerHand, deadDeck, pCardPos, displayFont, bet

    class splitButton(pygame.sprite.Sprite):
        """ Button that allows player to double (double the bet, take one more card, then stand)."""
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            self.image, self.rect = imageLoad("double-grey.png", 0)
            #self.position = (735, 330)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height/4 - 95)
            
        def update(self, mX, mY,   deck, deadDeck, playerHand, dealerHand, playerCards, cards, pCardPos, roundEnd, cardSprite, funds, bet, displayFont):
            """ If the button is clicked and the round is NOT over, let the player stand (take no more cards). """
            
            if roundEnd == 0 and funds >= bet * 2 and len(playerHand) == 2 and playerCards[0][1:] == playerCards[1][1:]: self.image, self.rect = imageLoad("double.png", 0)
            else: self.image, self.rect = imageLoad("double-grey.png", 0)
                
            #self.position = (735, 330)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height/4 - 95)
            self.rect.center = self.position
                
            if self.rect.collidepoint(mX, mY) == 1:
                if roundEnd == 0 and funds >= bet * 2 and len(playerHand) == 2: 
                    bet = bet * 2
                    
                    playClick()
                    deck, deadDeck, playerHand = hit(deck, deadDeck, playerHand)

                    currentCard = len(playerHand) - 1
                    card = cardSprite(playerHand[currentCard], pCardPos)
                    playerCards.add(card)
                    pCardPos = (pCardPos[0] - 80, pCardPos[1])
                    actionsList.append("Double")
                    deck, deadDeck, roundEnd, funds, displayFont = compareHands(deck, deadDeck, playerHand, dealerHand, funds, bet, cards, cardSprite)
                    
                    bet = bet / 2

            return deck, deadDeck, roundEnd, funds, playerHand, deadDeck, pCardPos, displayFont, bet

    class dealButton(pygame.sprite.Sprite):
        """ A button on the right hand side of the screen that can be clicked at the end of a round to deal a
        new hand of cards and continue the game. """
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            self.image, self.rect = imageLoad("deal.png", 0)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height) 
            #self.position[0] = self.position[0] - 10
            #self.position[1] = self.position[1] + 10
            #self.position = screen(735, 450)

        def update(self, mX, mY, deck, deadDeck, roundEnd, cardSprite, cards, playerHand, dealerHand, dCardPos, pCardPos, displayFont, playerCards, click, handsPlayed):
            """ If the mouse position collides with the button, and the mouse is clicking, and roundEnd does not = 0,
            then Calls deckDeal to deal a hand to the player and a hand to the dealer. It then
            takes the cards from the player's hand and the dealer's hand and creates sprites for them,
            placing them on the visible table. The deal button can only be pushed after the round has ended
            and a winner has been declared. """
            
            # Get rid of the in between-hands chatter
            textFont = pygame.font.Font(None, 28)
            
            if roundEnd == 1: self.image, self.rect = imageLoad("deal.png", 0)
            else: self.image, self.rect = imageLoad("deal-grey.png", 0)
            
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 10, screen.get_rect().midbottom[0] + self.rect.height)
            #self.position = screen(735, 450)
            self.rect.center = self.position
            
                
            if self.rect.collidepoint(mX, mY) == 1:
                if roundEnd == 1 and click == 1:
                    playClick()
                    displayFont = display(textFont, "")
                    actionsList.append("Deal")
                    cards.empty()
                    playerCards.empty()
                    
                    deck, deadDeck, playerHand, dealerHand = deckDeal(deck, deadDeck)

                    dCardPos = (50, 70)
                    pCardPos = (540,370)

                    # Create player's card sprites
                    for x in playerHand:
                        card = cardSprite(x, pCardPos)
                        pCardPos = (pCardPos[0] - 80, pCardPos [1])
                        playerCards.add(card)
                    
                    # Create dealer's card sprites  
                    faceDownCard = cardSprite("back", dCardPos)
                    dCardPos = (dCardPos[0] + 80, dCardPos[1])
                    cards.add(faceDownCard)

                    card = cardSprite(dealerHand [0], dCardPos)
                    cards.add(card)
                    roundEnd = 0
                    click = 0
                    handsPlayed += 1
                    
            return deck, deadDeck, playerHand, dealerHand, dCardPos, pCardPos, roundEnd, displayFont, click, handsPlayed
            
            
    class betButtonUp(pygame.sprite.Sprite):
        """ Button that allows player to increase his bet (in between hands only). """
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            self.image, self.rect = imageLoad("up.png", 0)
            #self.position = (710, 255)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width - 40, screen.get_rect().midbottom[0] + self.rect.height - 250)
            
        def update(self, mX, mY, bet, funds, click, roundEnd):
            if roundEnd == 1: self.image, self.rect = imageLoad("up.png", 0)
            else: self.image, self.rect = imageLoad("up-grey.png", 0)
            
            self.position = (screen.get_rect().midbottom[0] - self.rect.width - 40, screen.get_rect().midbottom[0] + self.rect.height - 250)
            #self.position = (710, 255)
            self.rect.center = self.position
            
            if self.rect.collidepoint(mX, mY) == 1 and click == 1 and roundEnd == 1:
                playClick()
                    
                if bet < funds:
                    bet += 5.0
                    # If the bet is not a multiple of 5, turn it into a multiple of 5
                    # This can only happen when the player has gotten blackjack, and has funds that are not divisible by 5,
                    # then loses money, and has a bet higher than his funds, so the bet is pulled down to the funds, which are uneven.
                    # Whew!
                    if bet % 5 != 0:
                        while bet % 5 != 0:
                            bet -= 1

                click = 0
            
            return bet, click
            
    class betButtonDown(pygame.sprite.Sprite):
        """ Button that allows player to decrease his bet (in between hands only). """
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            self.image, self.rect = imageLoad("down.png", 0)
            #self.position = (710, 255)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().midbottom[0] + self.rect.height - 250)
            
        def update(self, mX, mY, bet, click, roundEnd):  
            if roundEnd == 1: self.image, self.rect = imageLoad("down.png", 0)
            else: self.image, self.rect = imageLoad("down-grey.png", 0)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().midbottom[0] + self.rect.height - 250)
            #self.position = (760, 255)
            self.rect.center = self.position
            
            if self.rect.collidepoint(mX, mY) == 1 and click == 1 and roundEnd == 1:
                playClick()
                if bet > 5:
                    bet -= 5.0
                    if bet % 5 != 0:
                        while bet % 5 != 0:
                            bet += 1
                    
                click = 0
            
            return bet, click

    class saveArtButton(pygame.sprite.Sprite):
        """ Button that allows player to decrease his bet (in between hands only). """
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            font = pygame.font.Font(None, 28)
            textImage = font.render('Save Art', True,  (255,255,255), (0,0,255))
            self.image = textImage
            self.rect = textImage.get_rect()
            #self.image, self.rect = imageLoad("down.png", 0)
            #self.position = (710, 255)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().top + 50)
            
        def update(self, mX, mY, click):  
            #if roundEnd == 1: self.image, self.rect = imageLoad("down.png", 0)
            font = pygame.font.Font(None, 28)
            textImage = font.render('Save Art', True,  (255,255,255), (0,0,255))
            self.image = textImage
            self.rect = textImage.get_rect()
            #self.image, self.rect = imageLoad("down.png", 0)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().top + 50)
            #self.position = (760, 255)
            self.rect.center = self.position
            
            if self.rect.collidepoint(mX, mY) == 1 and click == 1:
                playClick()
                rec = screen.get_rect()
                subrect = Rect(rec.midbottom[0], rec.top, rec.width / 2, rec.height)
                
                artScreen = screen.subsurface(subrect)
                pygame.image.save(artScreen, "art.png")

                originalStdout = sys.stdout
                with open("history.txt", "w") as f:
                    sys.stdout = f
                    for item in histList:
                        print(item)
                    #print(histList)
                    sys.stdout = originalStdout
                    
                click = 0
            
            return click

    class abstractArtButton(pygame.sprite.Sprite):
        """ Button that allows player to decrease his bet (in between hands only). """
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            font = pygame.font.Font(None, 28)
            textImage = font.render('Abstract', True,  (255,255,255), (0,0,255))
            self.image = textImage
            self.rect = textImage.get_rect()
            #self.image, self.rect = imageLoad("up.png", 0)
            #self.position = (710, 255)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().top + 155)
            
        def update(self, mX, mY, click, artStyle):  
            #if roundEnd == 1: self.image, self.rect = imageLoad("down.png", 0)
            font = pygame.font.Font(None, 28)
            textImage = font.render('Abstract', True,  (255,255,255), (0,0,255))
            self.image = textImage
            self.rect = textImage.get_rect()
            #self.image, self.rect = imageLoad("up.png", 0)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().top + 155)
            #self.position = (760, 255)
            self.rect.center = self.position
            
            if self.rect.collidepoint(mX, mY) == 1 and click == 1:
                playClick()
                if artStyle == 0: #Tried to click when already on this style
                    click = 0
                    return click, artStyle
                artStyle = 0

                #Clear visual area
                scrRec = screen.get_rect()
                startBox = []
                border_width = 5
                visualArea = Rect((scrRec.midtop[0], scrRec.midtop[1]),
                                  (scrRec.width - scrRec.midtop[0], scrRec.height - scrRec.midtop[1]))
                # inner = Rect((scrRec.midtop[0] + border_width / 2, scrRec.midtop[1] + border_width / 2),
                # (scrRec.width - scrRec.midtop[0] - border_width, scrRec.height - scrRec.midtop[1] - border_width))
                screen.fill(WHITE, visualArea)
                innerBox = drawBorderedBox(screen, visualArea, border_width, (0,0,0), (255,255,255))
                startBox.append(innerBox)

                # Set up initial layout using histList
                for elem in histList:
                    #actionHistory = lastAction + " Player Cards: " + str(playerHand) + " Dealer Cards: " + str(dealerHand) + " WinChance: " + str(chanceToWin) + " BustChance: " + str(chanceToBust) + " Bet: " + str(bet) + " Funds: " + str(funds) + "\n"
                    parsedElem = parse("{lastAction} Player Cards: {playerHand} Dealer Cards: {dealerHand} WinChance: {chanceToWin} BustChance: {chanceToBust} Bet: {bet} Funds: {funds}\n", str(elem))
                    dHand = parsedElem['dealerHand']
                    dHand = dHand.replace("'", "")
                    trueDHand = dHand.strip('][').split(', ')
                    #print(trueDHand)
                    pHand = parsedElem['playerHand']
                    pHand = pHand.replace("'", "")
                    truePHand = pHand.strip('][').split(', ')
                    #print(truePHand)
                    cToW = float(parsedElem['chanceToWin'])
                    #print(cToW)
                    cToB = float(parsedElem['chanceToBust'])
                    #print(cToB)
                    betAmt = int(parsedElem['bet'])
                    #print(betAmt)
                    fundAmt = int(parsedElem['funds'])
                    #print(fundAmt)
                    drawAbstractArt(trueDHand, truePHand, cToW, cToB, betAmt, fundAmt, parsedElem['lastAction'])
                    #print(parsedElem)
                    
                click = 0
            
            return click, artStyle


    class mondrianArtButton(pygame.sprite.Sprite):
        """ Button that allows player to decrease his bet (in between hands only). """
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            font = pygame.font.Font(None, 28)
            textImage = font.render('Mondrian', True,  (255,255,255), (0,0,255))
            self.image = textImage
            self.rect = textImage.get_rect()
            #self.image, self.rect = imageLoad("up.png", 0)
            #self.position = (710, 255)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().top + 175)
            
        def update(self, mX, mY, click, artStyle):  
            #if roundEnd == 1: self.image, self.rect = imageLoad("down.png", 0)
            font = pygame.font.Font(None, 28)
            textImage = font.render('Mondrian', True,  (255,255,255), (0,0,255))
            self.image = textImage
            self.rect = textImage.get_rect()
            #self.image, self.rect = imageLoad("up.png", 0)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().top + 175)
            #self.position = (760, 255)
            self.rect.center = self.position
            
            if self.rect.collidepoint(mX, mY) == 1 and click == 1:
                playClick()
                if artStyle == 1: #Tried to click when already on this style
                    click = 0
                    return click, artStyle
                artStyle = 1

                #Clear visual area
                scrRec = screen.get_rect()
                startBox = []
                border_width = 5
                visualArea = Rect((scrRec.midtop[0], scrRec.midtop[1]),
                                  (scrRec.width - scrRec.midtop[0], scrRec.height - scrRec.midtop[1]))
                # inner = Rect((scrRec.midtop[0] + border_width / 2, scrRec.midtop[1] + border_width / 2),
                # (scrRec.width - scrRec.midtop[0] - border_width, scrRec.height - scrRec.midtop[1] - border_width))
                screen.fill(WHITE, visualArea)
                innerBox = drawBorderedBox(screen, visualArea, border_width, (0,0,0), (255,255,255))
                startBox.append(innerBox)

                pickle.dump(startBox, open('boxes.pkl', 'wb'))
                # Set up initial layout using histList
                for elem in histList:
                    #actionHistory = lastAction + " Player Cards: " + str(playerHand) + " Dealer Cards: " + str(dealerHand) + " WinChance: " + str(chanceToWin) + " BustChance: " + str(chanceToBust) + " Bet: " + str(bet) + " Funds: " + str(funds) + "\n"
                    parsedElem = parse("{lastAction} Player Cards: {playerHand} Dealer Cards: {dealerHand} WinChance: {chanceToWin} BustChance: {chanceToBust} Bet: {bet} Funds: {funds}\n", str(elem))
                    dHand = parsedElem['dealerHand']
                    dHand = dHand.replace("'", "")
                    trueDHand = dHand.strip('][').split(', ')
                    #print(trueDHand)
                    pHand = parsedElem['playerHand']
                    pHand = pHand.replace("'", "")
                    truePHand = pHand.strip('][').split(', ')
                    #print(truePHand)
                    cToW = float(parsedElem['chanceToWin'])
                    #print(cToW)
                    cToB = float(parsedElem['chanceToBust'])
                    #print(cToB)
                    betAmt = int(parsedElem['bet'])
                    #print(betAmt)
                    fundAmt = int(parsedElem['funds'])
                    #print(fundAmt)
                    drawMondrianArt(trueDHand, truePHand, cToW, cToB, betAmt, fundAmt, parsedElem['lastAction'])
                    #print(parsedElem)
                    
                click = 0
            
            return click, artStyle

    class linearCirclesArtButton(pygame.sprite.Sprite):
        """ Button that allows player to decrease his bet (in between hands only). """
        
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            font = pygame.font.Font(None, 28)
            textImage = font.render('Linear Circles', True,  (255,255,255), (0,0,255))
            self.image = textImage
            self.rect = textImage.get_rect()
            #self.image, self.rect = imageLoad("up.png", 0)
            #self.position = (710, 255)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().top + 195)
            
        def update(self, mX, mY, click, artStyle, lastY):  
            #if roundEnd == 1: self.image, self.rect = imageLoad("down.png", 0)
            font = pygame.font.Font(None, 28)
            textImage = font.render('Linear Circles', True,  (255,255,255), (0,0,255))
            self.image = textImage
            self.rect = textImage.get_rect()
            #self.image, self.rect = imageLoad("up.png", 0)
            self.position = (screen.get_rect().midbottom[0] - self.rect.width/2 - 20, screen.get_rect().top + 195)
            #self.position = (760, 255)
            self.rect.center = self.position
            
            if self.rect.collidepoint(mX, mY) == 1 and click == 1:
                playClick()
                if artStyle == 2: #Tried to click when already on this style
                    click = 0
                    return click, artStyle, lastY 
                artStyle = 2
                lastY = 0

                #Clear visual area
                scrRec = screen.get_rect()
                startBox = []
                border_width = 5
                visualArea = Rect((scrRec.midtop[0], scrRec.midtop[1]),
                                  (scrRec.width - scrRec.midtop[0], scrRec.height - scrRec.midtop[1]))
                # inner = Rect((scrRec.midtop[0] + border_width / 2, scrRec.midtop[1] + border_width / 2),
                # (scrRec.width - scrRec.midtop[0] - border_width, scrRec.height - scrRec.midtop[1] - border_width))
                screen.fill(WHITE, visualArea)
                innerBox = drawBorderedBox(screen, visualArea, border_width, (0,0,0), (255,255,255))
                startBox.append(innerBox)

                for elem in histList:
                    #actionHistory = lastAction + " Player Cards: " + str(playerHand) + " Dealer Cards: " + str(dealerHand) + " WinChance: " + str(chanceToWin) + " BustChance: " + str(chanceToBust) + " Bet: " + str(bet) + " Funds: " + str(funds) + "\n"
                    parsedElem = parse("{lastAction} Player Cards: {playerHand} Dealer Cards: {dealerHand} WinChance: {chanceToWin} BustChance: {chanceToBust} Bet: {bet} Funds: {funds}\n", str(elem))
                    dHand = parsedElem['dealerHand']
                    dHand = dHand.replace("'", "")
                    trueDHand = dHand.strip('][').split(', ')
                    #print(trueDHand)
                    pHand = parsedElem['playerHand']
                    pHand = pHand.replace("'", "")
                    truePHand = pHand.strip('][').split(', ')
                    #print(truePHand)
                    cToW = float(parsedElem['chanceToWin'])
                    #print(cToW)
                    cToB = float(parsedElem['chanceToBust'])
                    #print(cToB)
                    betAmt = int(parsedElem['bet'])
                    #print(betAmt)
                    fundAmt = int(parsedElem['funds'])
                    #print(fundAmt)
                    lastY = drawLinearCirclesArt(trueDHand, truePHand, cToW, cToB, betAmt, fundAmt, parsedElem['lastAction'], lastY)
                    #print(parsedElem)
                # Set up initial layout using histList
                    
                click = 0
            
            return click, artStyle, lastY
    ###### SPRITE FUNCTIONS END ######
         

    def drawAbstractArt(dealerHand, playerHand, chanceToWin, chanceToBust, bet, funds, lastAction):
        """Making triangle"""
        bColor = blueColorDict[lastAction]
        rColor = (chanceToBust/100 * 255) % 256
        

        """if playerValue > 21:
            rColor = 255
        elif playerValue == 21:
            rColor = 0
        else:
            rColor = (playerValue / 21) * 255"""

        gColor = (chanceToWin/100 * 255) % 256
        alpha = (chanceToBust/100 * 255) % 256
        if alpha < 20:
            if playerValue == 21:
                alpha = 255
            else:
                alpha = 20
        genColor = (rColor, gColor, bColor, alpha)

        #Getting limits
        maxRangeX = scrRec.right - scrRec.midtop[0]
        shiftX = scrRec.midtop[0]
        maxRangeY = scrRec.bottom
            


        point1x = (hash(str(dealerHand)) % maxRangeX) + shiftX
        dealerHandRev = str(dealerHand)[::-1]
        point1y = (hash(dealerHandRev) % maxRangeY)
        point1 = (point1x, point1y)
            
        point2x = (hash(str(playerHand)) % maxRangeX) + shiftX
        playerHandRev = str(playerHand)[::-1]
        point2y = (hash(playerHandRev) % maxRangeY)
        point2 = (point2x, point2y)
            
        # Bet already deducted from funds so add back
        tempFunds = funds
        if bet > funds: 
            tempFunds = bet + funds
        rangeX = bet/tempFunds * chanceToWin/100 * maxRangeX
        point3x = rangeX + shiftX
        point3y = (bet/tempFunds * chanceToBust/100 * maxRangeY)
        point3 = (point3x, point3y)

        points = [point1, point2, point3]
            
        #Drawing to intermediateSurface to mask with screen
        #intermediateSurface = pygame.Surface((scrRec.width, scrRec.height))
        #white = (255, 255, 255)
        #whiteRect = Rect(screenRect.midtop[0], screenRect.midtop[1], screenRect.topright[0] - screenRect.midtop[0], screenRect.midbottom[1])
        #pygame.draw.rect(intermediateSurface, white, whiteRect)
        #intermediateSurface.fill((255, 255, 255))
        pygame.gfxdraw.filled_polygon(screen, points, genColor)
        return

    

    def drawLinearCirclesArt(dHand, pHand, chanceToWin, chanceToBust, bet, funds, lastAction, lastY):
        """ Linear Circle Art """
        #Positions of points for polygon
        mid = screenRect.midtop[0]
        minDistance = (screenRect.right - mid) / 10 #Always take up a 10th of side screen
        tempFunds = funds
            
        # Bet already deducted from funds so add back
        if bet > funds: 
            tempFunds = bet + funds
        totalDistance = minDistance + int((bet/tempFunds * (screenRect.right - mid - minDistance)) )
        midX = (mid + screenRect.right) / 2
        leftX = midX - (totalDistance/2)
        rightX = midX + (totalDistance/2)
        newY = lastY + 30 #Const for now but %of screen would likely be better
        #point1 = (leftX, lastY)
        #point2 = (rightX, lastY)
        #Could inverse the leftX and rightX to make more interesting shapes
        #point3 = (rightX, newY)
        #point4 = (leftX, newY)

        #points = [point1, point2, point3, point4]

        #Color for polygon
        #Placeholder for winChance
        
        playerValue = checkValue(pHand)
        #winChance = playerValue <= 21 ? playerValue / 21 : 0 
        #chanceToWin = random.random()
        gValue = (chanceToWin/100 * 255) % 256 
        rValue = hash(str(dHand)) % 256
        bValue = hash(str(pHand)) % 256

        alpha = (chanceToBust/100 * 255) % 256
        if alpha < 20:
            if playerValue == 21:
                alpha = 255
            else:
                alpha = 20

        genColor = (rValue, gValue, bValue, alpha)

        #Background polygon
        bgValue = 0
        brValue = 0
        if (funds >= 100):
            #Made money from game
            bgValue = (funds - 100) % 256 
        else:
            #Lost money since start
            brValue = (funds / 100) * 255   
        #bpoint1 = (mid, lastY)
        #bpoint2 = (screenRect.right, lastY)
        #bpoint3 = (screenRect.right, newY)
        #bpoint4 = (mid, newY)
        #bColor = (brValue, bgValue, 0)

        #bpoints = [bpoint1, bpoint2, bpoint3, bpoint4]

        #draw
        #pygame.gfxdraw.filled_polygon(screen, bpoints, bColor)
        lowest_x=mid+totalDistance
        highest_x=screenRect.right-totalDistance
        lowest_y=screenRect.bottom+totalDistance
        highest_y=screenRect.top-totalDistance
        rangeX = highest_x - lowest_x
        offsetX = int((chanceToWin/100) * rangeX)
        pygame.gfxdraw.filled_circle(screen,offsetX + int(lowest_x),newY, int(totalDistance), genColor)
        #Reset lastY
        lastY = newY + 1 #+1 for slight spacing
            
        """ Linear Circle Art End """
        return lastY

    def drawMondrianArt(dealerHand, playerHand, chanceToWin, chanceToBust, bet, funds, lastAction):
        startBox = pickle.load(open('boxes.pkl', "rb"))
        #print(startBox)

        #print('GLobal' + str(x))
        actionCode = 0
        # The action determines color
        if lastAction == 'Deal':
            actionCode = 3
        elif lastAction == 'Stand':
            actionCode = 0
        elif lastAction == 'Hit':
            actionCode = 1
        elif lastAction == 'Double':
            actionCode = 2
        cr = COLORS[actionCode]
        rect = startBox.pop(0)
        # print(chanceToWin)
        # print(chanceToBust)
        if int(chanceToBust) == 0:
            chanceToBust = 33  # minimum
        if int(chanceToWin) == 0:
            chanceToWin = 33  # minimum
            print('o win')
        if int(chanceToWin) == int(chanceToBust):
            chanceToBust = 67
            chanceToWin = 33

        smallBoxSizeFactor = (min(chanceToBust, chanceToWin) + max(chanceToBust, chanceToWin)) / 2 / 100
        if smallBoxSizeFactor < 0.1:
            smallBoxSizeFactor = (33+67)/2
        smallBoxW = smallBoxSizeFactor * rect.width
        smallBoxH = smallBoxSizeFactor * rect.height
        smallBoxXFactor = (int(chanceToBust)) / 2 / 100
        smallBoxYFactor = (int(chanceToWin)) / 2 / 100
        smallBoxX = rect.x + smallBoxXFactor * rect.width
        smallBoxY = rect.y + smallBoxYFactor * rect.height
        smallBoxVis = Rect((smallBoxX, smallBoxY), (smallBoxW, smallBoxH))

        smallBox = drawBorderedBox(screen, smallBoxVis, border_width, BLACK, cr)
        if smallBoxX > rect.x:
            restRect = Rect((rect.x, rect.y),
                            (smallBoxX - rect.x, smallBoxY + smallBoxH - rect.y))

            startBox.append(restRect)
        if smallBoxX + smallBoxW < rect.x + rect.width:
            restRect = Rect((smallBoxX + smallBoxW, smallBoxY),
                            (rect.x + rect.width - smallBoxX - smallBoxW, rect.height + rect.y - smallBoxY))
            startBox.append(restRect)
            print(1)
        if smallBoxY + smallBoxH < rect.y + rect.height:
            restRect = Rect((rect.x, smallBoxY + smallBoxH),
                            (smallBoxX + smallBoxW - rect.x, rect.y + rect.height - smallBoxY - smallBoxH))
            startBox.append(restRect)
            print(2)
        if smallBoxY > rect.y:
            restRect = Rect((smallBoxX, rect.y), (rect.x + rect.width - smallBoxX, smallBoxY - rect.y))
            startBox.append(restRect)
            print(3)
        # print(smallBox)
        """ Mondrian End """
        pickle.dump(startBox, open('boxes.pkl', 'wb'))
        return



    ###### INITIALIZATION BEGINS ######
    # This font is used to display text on the right-hand side of the screen
    textFont = pygame.font.Font(None, 28)

    # This sets up the background image, and its container rect
    #background, backgroundRect = imageLoad("bjs.png", 0)
    
    #Split the window into two sections
    white = (255, 255, 255)
    screenRect = screen.get_rect()
    lastY = screenRect.top
    whiteRect = Rect(screenRect.midtop[0], screenRect.midtop[1], screenRect.topright[0] - screenRect.midtop[0], screenRect.midbottom[1])
    pygame.draw.rect(screen, white, whiteRect)
    pygame.display.flip()

    

    # cards is the sprite group that will contain sprites for the dealer's cards
    cards = pygame.sprite.Group()
    # playerCards will serve the same purpose, but for the player
    playerCards = pygame.sprite.Group()

    # This creates instances of all the button sprites
    bbU = betButtonUp()
    bbD = betButtonDown()
    standButton = standButton()
    dealButton = dealButton()
    hitButton = hitButton()
    doubleButton = doubleButton()
    saveButton = saveArtButton()
    abstractButton = abstractArtButton()
    mondrianButton = mondrianArtButton()
    linearCirclesButton = linearCirclesArtButton()
    #splButton = splitButton()
    
    # This group containts the button sprites
    buttons = pygame.sprite.Group(bbU, bbD, hitButton, standButton, dealButton, doubleButton, saveButton, abstractButton, mondrianButton, linearCirclesButton)#, splButton)

    # The 52 card deck is created
    deck = createDeck()
    # The dead deck will contain cards that have been discarded
    deadDeck = []

    # These are default values that will be changed later, but are required to be declared now
    # so that Python doesn't get confused
    playerHand, dealerHand, dCardPos, pCardPos = [],[],(),()
    mX, mY = 0, 0
    click = 0

    # The default funds start at $100.00, and the initial bet defaults to $10.00
    funds = 100.00
    bet = 10.00

    #Tracks the history of the game 
    histList = []

    # This is a counter that counts the number of rounds played in a given session
    handsPlayed = 0

    # This is a counter of total actions that have been taken in a given session
    actionsTaken = 0

    # When the cards have been dealt, roundEnd is zero.
    #In between rounds, it is equal to one
    roundEnd = 1
    
    # firstTime is a variable that is only used once, to display the initial
    # message at the bottom, then it is set to zero for the duration of the program.
    firstTime = 1

    #Dictionary for Abstract Mosiac Art
    blueColorDict = {
        "Double": 255,
        "Stand": 255,
        "PlayerBlackjack": 255,
        "DealerBlackjack": 255,
        "Hit": 128,
        "Deal": 0
    }

    COLORS = ((255, 1, 1), (255, 240, 1), (1, 1, 253), (249, 249, 249))
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    
    #Default art style
    artStyle = 2

    ###### INITILIZATION ENDS ########
    
    ###### MAIN GAME LOOP BEGINS #######
    scrRec = screen.get_rect()

    starterBox = []
    border_width = 5
    visualArea = Rect((scrRec.midtop[0], scrRec.midtop[1]),
                      (scrRec.width - scrRec.midtop[0], scrRec.height - scrRec.midtop[1]))
    # inner = Rect((scrRec.midtop[0] + border_width / 2, scrRec.midtop[1] + border_width / 2),
    # (scrRec.width - scrRec.midtop[0] - border_width, scrRec.height - scrRec.midtop[1] - border_width))
    screen.fill(WHITE, visualArea)
    innerBox = drawBorderedBox(screen, visualArea, border_width, BLACK, WHITE)
    starterBox.append(innerBox)
    pickle.dump(starterBox, open("boxes.pkl", "wb"))  # save it again
    #print(startBox)

    while 1:

        greenBackground = Rect((0, 0), (scrRec.width * 2/5, scrRec.height - 30))
        pygame.draw.rect(screen, (0, 255, 0), greenBackground)
        #screen.blit(background, backgroundRect)
        
        if bet > funds:
            # If you lost money, and your bet is greater than your funds, make the bet equal to the funds
            bet = funds
        
        if roundEnd == 1 and firstTime == 1:
            # When the player hasn't started. Will only be displayed the first time.
            #buttonClicked = pyautogui.confirm(text='you have done this', title='How to play Blackjack')
            displayFont = display(textFont, "Click on the arrows to declare your bet, then deal to start the game.")
            firstTime = 0
            
        # Show the blurb at the bottom of the screen, how much money left, and current bet   
        #self.position = (screen.get_rect().midbottom[0] - 20, screen.get_rect().midbottom[0] - 200)
        blackRect = Rect(scrRec.bottomleft[0], scrRec.bottomleft[1] - 325, scrRec.midbottom[0], 325) 
        pygame.draw.rect(screen, (0, 0, 0), blackRect)
        screen.blit(displayFont, (scrRec.bottomleft[0] + 20, scrRec.bottomleft[1] - 30))
        #screen.blit(displayFont, (10,444))
        fundsFont = pygame.font.Font.render(textFont, "Funds: $%.2f" %(funds), 1, (255,255,255), (0,0,0))
        screen.blit(fundsFont, (scrRec.midbottom[0] - 150, scrRec.midbottom[1] - 400) )
        #screen.blit(fundsFont, (663,205))
        betFont = pygame.font.Font.render(textFont, "Bet: $%.2f" %(bet), 1, (255,255,255), (0,0,0))
        screen.blit(betFont, (scrRec.midbottom[0] - 150, scrRec.midbottom[1] - 375) )
        #screen.blit(betFont, (680,285))
        hpFont = pygame.font.Font.render(textFont, "Round: %i " %(handsPlayed), 1, (255,255,255), (0,0,0))
        screen.blit(hpFont, (scrRec.midbottom[0] - 150, scrRec.midbottom[1] - 425) )
        #screen.blit(hpFont, (663, 180))

        #Tutorial (Render doesn't allow \n so have to render each separately)
        tutorialStr = []
        tutorialStr.append('How to Use Probability Art and Play Blackjack') 
        tutorialStr.append('Adjusting Bet: Before the game starts, use the arrows in the middle of the screen to adjust your bet.') 
        tutorialStr.append('Starting Game: The game starts once "Deal" is pressed.')
        tutorialStr.append('Deal: The Dealer is dealt two cards, one face up and one face down, and the player is dealt two cards.' )
        tutorialStr.append('Now there are three options' )
        tutorialStr.append('    Hit: Get dealt another card')
        tutorialStr.append('    Stand: Your turn ends, and the dealer takes their turn' )
        tutorialStr.append('    Double: Double Down on your dealt hand by doubling your bet and only receiving one more card before your turn ends.' )
        tutorialStr.append('        You can only use this at the start of a round)') 
        tutorialStr.append('Cards Value:') 
        tutorialStr.append('    A = 1 or 11 (Highest without exceeding 21)') 
        tutorialStr.append('    2-10 = 2-10 (Value of number)') 
        tutorialStr.append('    J,Q,K = 10') 
        tutorialStr.append('    Hand value is added total of each card in hand') 
        tutorialStr.append('Dealer Turn: Once you end your turn, the dealer will take their turn by hitting until their hand total is equal to or greater than 17')
        tutorialStr.append('Round End Conditions (After player turn and dealer turn or ends round immediately)') 
        tutorialStr.append('    Busting: If your hand total goes over 21, you lose immediately and the same rule applies to dealer')
        tutorialStr.append('    Passing: If your hand total equals the dealer hand total, you Pass in tie and bet is returned') 
        tutorialStr.append('    Blackjack: If starting dealt cards equal 21 and dealer doesnt, you have Blackjack and receive x1.5 your placed bet')
        tutorialStr.append('    To Win: Try to get your hand total higher than the dealer hand total without going over 21') 
        tutorialStr.append('        Receive x1 of placed bet on win, but lose inital placed bet on lose') 
        tutorialStr.append('Save Image: Click Save Art button at the top middle to save current art as art.png and history of states in history.txt')
        tutorialStr.append('Change Style: Click any of the three style button below Save Art to recreate art in different style')
        smallFont = pygame.font.Font(None, 20)
        textOffset = 0
        for tStr in tutorialStr:
            tutorialFont = pygame.font.Font.render(smallFont, tStr, 1, (255,255,255), (0,0,0))
            screen.blit(tutorialFont, (scrRec.bottomleft[0], scrRec.bottomleft[1] - 325 + textOffset) )
            textOffset += 12
        """tutorialFont = pygame.font.Font.render(smallFont, tutorialStr1, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 450) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr2, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 440) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr3, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 430) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr4, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 420) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr5, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 410) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr6, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 400) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr7, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 390) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr8, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 380) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr9, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 370) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr10, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 360) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr11, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 350) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr12, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 5, scrRec.bottomleft[1] - 340) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr13, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 10, scrRec.bottomleft[1] - 330) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr14, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 10, scrRec.bottomleft[1] - 320) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr15, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 10, scrRec.bottomleft[1] - 310) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr16, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 10, scrRec.bottomleft[1] - 300) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr17, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 10, scrRec.bottomleft[1] - 290) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr18, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 10, scrRec.bottomleft[1] - 280) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr19, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 10, scrRec.bottomleft[1] - 270) )
        tutorialFont = pygame.font.Font.render(smallFont, tutorialStr20, 1, (255,255,255), (0,0,0))
        screen.blit(tutorialFont, (scrRec.bottomleft[0] + 10, scrRec.bottomleft[1] - 260) )"""





        for event in pygame.event.get():
            if event.type == QUIT:
                sys.exit()
            elif event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    mX, mY = pygame.mouse.get_pos()
                    click = 1
            elif event.type == MOUSEBUTTONUP:
                mX, mY = 0, 0
                click = 0
            if event.type == KEYDOWN and event.key == K_ESCAPE:
                #print(histList)
                pygame.quit()
                sys.exit()
            
        # Initial check for the value of the player's hand, so that his hand can be displayed and it can be determined
        # if the player busts or has blackjack or not
        if roundEnd == 0:
            # Stuff to do when the game is happening 
            playerValue = checkValue(playerHand)
            dealerValue = checkValue(dealerHand)
    
            if playerValue == 21 and len(playerHand) == 2:
                # If the player gets blackjack
                displayFont, playerHand, dealerHand, deadDeck, funds, roundEnd = blackJack(deck, deadDeck, playerHand, dealerHand, funds,  bet, cards, cardSprite)
                actionsList.append("PlayerBlackjack")
                
            if dealerValue == 21 and len(dealerHand) == 2:
                # If the dealer has blackjack
                displayFont, playerHand, dealerHand, deadDeck, funds, roundEnd = blackJack(deck, deadDeck, playerHand, dealerHand, funds,  bet, cards, cardSprite)
                actionsList.append("DealerBlackjack")
            if playerValue > 21:
                # If player busts
                deck, playerHand, dealerHand, deadDeck, funds, roundEnd, displayFont = bust(deck, playerHand, dealerHand, deadDeck, funds, 0, bet, cards, cardSprite)
         
        # Update the buttons 
        # deal 
        deck, deadDeck, playerHand, dealerHand, dCardPos, pCardPos, roundEnd, displayFont, click, handsPlayed = dealButton.update(mX, mY, deck, deadDeck, roundEnd, cardSprite, cards, playerHand, dealerHand, dCardPos, pCardPos, displayFont, playerCards, click, handsPlayed)   
        # hit    
        deck, deadDeck, playerHand, pCardPos, click = hitButton.update(mX, mY, deck, deadDeck, playerHand, playerCards, pCardPos, roundEnd, cardSprite, click)
        # stand    
        deck, deadDeck, roundEnd, funds, playerHand, deadDeck, pCardPos,  displayFont  = standButton.update(mX, mY,   deck, deadDeck, playerHand, dealerHand, cards, pCardPos, roundEnd, cardSprite, funds, bet, displayFont)
        # double
        deck, deadDeck, roundEnd, funds, playerHand, deadDeck, pCardPos, displayFont, bet  = doubleButton.update(mX, mY,   deck, deadDeck, playerHand, dealerHand, playerCards, cards, pCardPos, roundEnd, cardSprite, funds, bet, displayFont)
        # Split
        #deck, deadDeck, roundEnd, funds, playerHand, deadDeck, pCardPos, displayFont, bet  = splButton.update(mX, mY,   deck, deadDeck, playerHand, dealerHand, playerCards, cards, pCardPos, roundEnd, cardSprite, funds, bet, displayFont)
        # Bet buttons
        bet, click = bbU.update(mX, mY, bet, funds, click, roundEnd)
        bet, click = bbD.update(mX, mY, bet, click, roundEnd)

        # Save and styles
        click = saveButton.update(mX, mY, click)
        click, artStyle = abstractButton.update(mX, mY, click, artStyle)
        click, artStyle = mondrianButton.update(mX, mY, click, artStyle)
        click, artStyle, lastY = linearCirclesButton.update(mX, mY, click, artStyle, lastY)

        # draw them to the screen
        buttons.draw(screen)

        #Update art
        #actionList for actions (could remove elements to not deal with indexing)
        #actionsTaken used to properly space/place the polygons
        #May be helpful to keep track of last two points (could be initialized to initial points (may have issues with blackjack))
        #Using screenRect = screen.get_rect() and properties such as screenRect.midtop or just screenRect.mid
        #could be useful in positioning points
        #Rather than actionsTaken instead keep track of last y used
        if len(actionsList) > 0:
            lastAction = actionsList.pop()
            """Abstract Mosiac"""
            
            
            #Check round end and change playerHand accordingly
            if roundEnd == 1 or len(playerHand) == 0:
                #print(playerHandBeforeReset)
                #print(dealerHandBeforeReset)
                playerHand = playerHandBeforeReset
                dealerHand = dealerHandBeforeReset

            playerValue = checkValue(playerHand)
            dealerValue = checkValue(dealerHand)
            

            chanceToWin = winChance(deck, dealerHand, playerHand)
            chanceToBust = bustChance(deck, dealerHand, playerHand)
            #displayFont = display(textFont, "Chance to Win: " + str(chanceToWin) + " Chance to Bust: " + str(chanceToBust) )

            #displayFont = display(textFont, "Chance to Bust: " + str(chanceToBust) )
            blackRect = Rect(scrRec.bottomleft[0], scrRec.bottomleft[1] - 30, scrRec.midbottom[0], 30) 
            pygame.draw.rect(screen, (0, 0, 0), blackRect)
            screen.blit(displayFont, (scrRec.bottomleft[0] + 20, scrRec.bottomleft[1] - 30))
            #screen.blit(winChanceFont, (scrRec.topleft[0] + 20, scrRec.topleft[1] + 30))

            #chanceToWin = 1
            

            #dCardValuePos = (10, 10)
            #pCardValuePos = (20,20)
            dealerHandShowValue = 0
            #print("Calculating dealerHand value")
            if roundEnd == 1:
                #Use checkValue(dealerHand) since player would now know the dealers hand
                dealerHandShowValue = dealerValue
            else:
                value = dealerHand[0][1:]
                # Jacks, kings and queens are all worth 10, and ace is worth 11    
                if value == 'j' or value == 'q' or value == 'k': dealerHandShowValue = 10
                elif value == 'a': dealerHandShowValue = 11
                else: dealerHandShowValue = int(value)
            
            #dealerCardValue = display(textFont, str(dealerHandShowValue))
            #playerCardValue = display(textFont, str(playerValue))
            #dealerBlackRect = Rect(dCardValuePos, (50, 30)) 
            #pygame.draw.rect(screen, (0, 0, 0), dealerBlackRect)
            #playerBlackRect = Rect(pCardValuePos, (50, 30)) 
            #pygame.draw.rect(screen, (0, 0, 0), playerBlackRect)
            #print("Dealer Shown Total: {}".format(dealerHandShowValue))
            #print("Player Total: {}".format(playerValue))
            dealerFont = pygame.font.Font.render(textFont, "Dealer Total: %02d" %(dealerHandShowValue), 1, (255,255,255), (0,0,0))
            playerFont = pygame.font.Font.render(textFont, "Player Total: %02d" %(playerValue), 1, (255, 255, 255), (0,0,0))
            #screen.blit(fundsFont, (scrRec.midbottom[0] - 150, scrRec.midbottom[1] - 400) )
            screen.blit(dealerFont, (scrRec.midbottom[0] - 150, scrRec.midbottom[1] - 475))
            screen.blit(playerFont, (scrRec.midbottom[0] - 150, scrRec.midbottom[1] - 500))
            
            #Possibly add the expected action from table
            actionHistory = lastAction + " Player Cards: " + str(playerHand) + " Dealer Cards: " + str(dealerHand) + " WinChance: " + str(chanceToWin) + " BustChance: " + str(chanceToBust) + " Bet: " + str(int(bet)) + " Funds: " + str(int(funds)) + "\n"
            histList.append(actionHistory)

            if artStyle == 0:
                drawAbstractArt(dealerHand, playerHand, chanceToWin, chanceToBust, bet, funds, lastAction)
                """Making triangle"""
                #bColor = blueColorDict[lastAction]
                #rColor = 0

                #if playerValue > 21:
                #    rColor = 255
                #elif playerValue == 21:
                #    rColor = 0
                #else:
                #    rColor = (playerValue / 21) * 255

                #gColor = (chanceToWin * 255) % 256
                #alpha = (chanceToBust * 255) % 256
                #genColor = (rColor, gColor, bColor, alpha)

                ##Getting limits
                #maxRangeX = scrRec.right - scrRec.midtop[0]
                #shiftX = scrRec.midtop[0]
                #maxRangeY = scrRec.bottom
            


                #point1x = (hash(str(dealerHand)) % maxRangeX) + shiftX
                #dealerHandRev = str(dealerHand)[::-1]
                #point1y = (hash(dealerHandRev) % maxRangeY)
                #point1 = (point1x, point1y)
            
                #point2x = (hash(str(playerHand)) % maxRangeX) + shiftX
                #playerHandRev = str(playerHand)[::-1]
                #point2y = (hash(playerHandRev) % maxRangeY)
                #point2 = (point2x, point2y)
            
                ## Bet already deducted from funds so add back
                #tempFunds = funds
                #if bet > funds: 
                #    tempFunds = bet + funds
                #rangeX = bet/tempFunds * maxRangeX
                #point3x = rangeX + shiftX
                #point3y = (bet/tempFunds * maxRangeY)
                #point3 = (point3x, point3y)

                #points = [point1, point2, point3]
            
                ##Drawing to intermediateSurface to mask with screen
                ##intermediateSurface = pygame.Surface((scrRec.width, scrRec.height))
                ##white = (255, 255, 255)
                ##whiteRect = Rect(screenRect.midtop[0], screenRect.midtop[1], screenRect.topright[0] - screenRect.midtop[0], screenRect.midbottom[1])
                ##pygame.draw.rect(intermediateSurface, white, whiteRect)
                ##intermediateSurface.fill((255, 255, 255))
                #pygame.gfxdraw.filled_polygon(screen, points, genColor)

                #Get masks
                #intermedMask = pygame.mask.from_surface(intermediateSurface)
                #screenMask = pygame.mask.from_surface(screen)
            
                #Offsets
                #midpoint1 = ((point1[0] + point2[0])/2, (point1[1] + point2[1])/2)
                #midpoint2 = (((point1[0] + point3[0])/2, (point1[1] + point3[1])/2))
                #offset_x = 0#midpoint1[0]
                #offset_y = 0#midpoint1[1]
                #if screenMask.overlap(intermedMask, (offset_x, offset_y)):
                    #print("Intersection")
                    #print(intermedMask.overlap_area(screenMask, (offset_x, offset_y)))
                #    overlapMask = intermedMask.overlap_mask(screenMask, (offset_x, offset_y))
                #    overlapSurf = overlapMask.to_surface()
                    #screen.blit(overlapSurf, (0,0))
                #    overlapSurf.set_colorkey((0,0,0))
                #    overlapWidth, overlapHeight = overlapSurf.get_size()
                #    for x in range(overlapWidth):
                #        for y in range(overlapHeight):
                #            if overlapSurf.get_at(x, y)[0] != 0:
                #                screen.set_at((x, y), "red")
                
                #screen.blit(intermediateSurface, (0, 0))
                #pygame.gfxdraw.filled_polygon(screen, points, genColor)
                #screen.blit(overlapSurf, (0,0))
            
                #screen.blit(overlapSurf, (0,0)"""
            """Abstract Mosiac End"""

            """Mondrian"""
            #artStyle = 1
            if artStyle == 1:
                drawMondrianArt(dealerHand, playerHand, chanceToWin, chanceToBust, bet, funds, lastAction)
                ##print(lastAction)
                #actionCode = 0
                ## The action determines color
                #if lastAction == 'Deal':
                #    actionCode = 3
                #elif lastAction == 'Stand':
                #    actionCode = 0
                #elif lastAction == 'Hit':
                #    actionCode = 1
                #elif lastAction == 'Double':
                #    actionCode = 2
                #cr = COLORS[actionCode]
                #rect = startBox.pop(0)
                ##print(chanceToWin)
                ##print(chanceToBust)
                #if chanceToBust == 0:
                #    chanceToBust = random.randrange(33, 67)  # minimum
                #    #print('o bust')
                #if chanceToWin == 0:
                #    chanceToWin = random.randrange(33, 67)  # minimum
                #    #print('o win')
                #if chanceToWin == chanceToBust:
                #    chanceToBust = random.randrange(33, 67)
                #    chanceToWin = random.randrange(33, 67)

                #smallBoxSizeFactor = random.randrange(int(min(chanceToBust, chanceToWin)),
                #                                  int(max(chanceToBust, chanceToWin))) / 100
                #if smallBoxSizeFactor < 0.1:
                #    smallBoxSizeFactor = random.randrange(33, 67)/100
                #smallBoxW = smallBoxSizeFactor * rect.width
                #smallBoxH = smallBoxSizeFactor * rect.height
                #smallBoxXFactor = random.randrange(0, int(chanceToBust)) / 100
                #smallBoxYFactor = random.randrange(0, int(chanceToWin)) / 100
                #smallBoxX = rect.x + smallBoxXFactor * rect.width
                #smallBoxY = rect.y + smallBoxYFactor * rect.height
                #smallBoxVis = Rect((smallBoxX, smallBoxY), (smallBoxW, smallBoxH))

                #smallBox = drawBorderedBox(screen, smallBoxVis, border_width, BLACK, cr)
                #if smallBoxX > rect.x:
                #    restRect = Rect((rect.x, rect.y),
                #                (smallBoxX - rect.x, smallBoxY + smallBoxH - rect.y))

                #    startBox.append(restRect)
                #if smallBoxX + smallBoxW < rect.x + rect.width:
                #    restRect = Rect((smallBoxX + smallBoxW, smallBoxY),
                #                (rect.x + rect.width - smallBoxX - smallBoxW, rect.height + rect.y - smallBoxY))
                #    startBox.append(restRect)
                #    print(1)
                #if smallBoxY+smallBoxH < rect.y + rect.height:
                #    restRect = Rect((rect.x, smallBoxY + smallBoxH),
                #                (smallBoxX+smallBoxW-rect.x, rect.y + rect.height - smallBoxY - smallBoxH))
                #    startBox.append(restRect)
                #    print(2)
                #if smallBoxY > rect.y:
                #    restRect = Rect((smallBoxX, rect.y), (rect.x + rect.width-smallBoxX, smallBoxY-rect.y))
                #    startBox.append(restRect)
                #    print(3)
                ##print(startBox)
                ##print(smallBox)
            """ Mondrian End """

            if artStyle == 2:
                lastY = drawLinearCirclesArt(dealerHand, playerHand, chanceToWin, chanceToBust, bet, funds, lastAction, lastY)
            #    """ Linear Circle Art """
            #    #Positions of points for polygon
            #    mid = screenRect.midtop[0]
            #    minDistance = (screenRect.right - mid) / 10 #Always take up a 10th of side screen
            #    tempFunds = funds
            
            #    # Bet already deducted from funds so add back
            #    if bet > funds: 
            #        tempFunds = bet + funds
            #    totalDistance = minDistance + int((bet/tempFunds * (screenRect.right - mid - minDistance)) )
            #    midX = (mid + screenRect.right) / 2
            #    leftX = midX - (totalDistance/2)
            #    rightX = midX + (totalDistance/2)
            #    newY = lastY + 30 #Const for now but %of screen would likely be better
            #    #point1 = (leftX, lastY)
            #    #point2 = (rightX, lastY)
            #    #Could inverse the leftX and rightX to make more interesting shapes
            #    #point3 = (rightX, newY)
            #    #point4 = (leftX, newY)

            #    #points = [point1, point2, point3, point4]

            #    #Color for polygon
            #    #Placeholder for winChance
            #    playerValue = checkValue(playerHand)
            #    #winChance = playerValue <= 21 ? playerValue / 21 : 0 
            #    #chanceToWin = random.random()
            #    gValue = (chanceToWin * 255) % 256 
            #    rValue = hash(str(dealerHand)) % 256
            #    bValue = hash(str(playerHand)) % 256
            #    genColor = (rValue, gValue, bValue, random.randrange(0,255))

            #    #Background polygon
            #    bgValue = 0
            #    brValue = 0
            #    if (funds >= 100):
            #        #Made money from game
            #        bgValue = (funds - 100) % 256 
            #    else:
            #        #Lost money since start
            #        brValue = (funds / 100) * 255   
            #    #bpoint1 = (mid, lastY)
            #    #bpoint2 = (screenRect.right, lastY)
            #    #bpoint3 = (screenRect.right, newY)
            #    #bpoint4 = (mid, newY)
            #    #bColor = (brValue, bgValue, 0)

            #    #bpoints = [bpoint1, bpoint2, bpoint3, bpoint4]

            #    #draw
            #    #pygame.gfxdraw.filled_polygon(screen, bpoints, bColor)
            #    lowest_x=mid+totalDistance
            #    highest_x=screenRect.right-totalDistance
            #    lowest_y=screenRect.bottom+totalDistance
            #    highest_y=screenRect.top-totalDistance
            #    pygame.gfxdraw.filled_circle(screen,int(random.randrange(lowest_x,highest_x)),newY, int(totalDistance), genColor)
            #    #Reset lastY
            #    lastY = newY + 1 #+1 for slight spacing
            
            #""" Linear Circle Art End """ 

            
            """Linear Art
            #Positions of points for polygon
            mid = screenRect.midtop[0]
            minDistance = (screenRect.right - mid) / 10 #Always take up a 10th of side screen
            tempFunds = funds
            
            # Bet already deducted from funds so add back
            if bet > funds: 
                tempFunds = bet + funds
            totalDistance = minDistance + int((bet/tempFunds * (screenRect.right - mid - minDistance)) )
            midX = (mid + screenRect.right) / 2
            leftX = midX - (totalDistance/2)
            rightX = midX + (totalDistance/2)
            newY = lastY + 30 #Const for now but %of screen would likely be better
            point1 = (leftX, lastY)
            point2 = (rightX, lastY)
            #Could inverse the leftX and rightX to make more interesting shapes
            point3 = (rightX, newY)
            point4 = (leftX, newY)

            points = [point1, point2, point3, point4]

            #Color for polygon
            #Placeholder for winChance
            playerValue = checkValue(playerHand)
            #winChance = playerValue <= 21 ? playerValue / 21 : 0 
            winChance = random.random()
            gValue = winChance * 255 
            rValue = hash(str(dealerHand)) % 256
            bValue = hash(str(playerHand)) % 256
            genColor = (rValue, gValue, bValue)

            #Background polygon
            bgValue = 0
            brValue = 0
            if (funds >= 100):
                #Made money from game
                bgValue = (funds - 100) % 256 
            else:
                #Lost money since start
                brValue = (funds / 100) * 255   
            bpoint1 = (mid, lastY)
            bpoint2 = (screenRect.right, lastY)
            bpoint3 = (screenRect.right, newY)
            bpoint4 = (mid, newY)
            bColor = (brValue, bgValue, 0)

            bpoints = [bpoint1, bpoint2, bpoint3, bpoint4]

            #draw
            pygame.gfxdraw.filled_polygon(screen, bpoints, bColor)
            pygame.gfxdraw.filled_polygon(screen, points, genColor)

            #Reset lastY
            lastY = newY + 1 #+1 for slight spacing
            Linear End"""

            #Check round end and change playerHand accordingly
            """if roundEnd == 1:
                playerHand.empty()
                dealerHand.empty()"""
         
        # If there are cards on the screen, draw them    
        if len(cards) != 0:
            playerCards.update()
            playerCards.draw(screen)
            cards.update()
            cards.draw(screen)

        # Updates the contents of the display
        pygame.display.flip()
    ###### MAIN GAME LOOP ENDS ######
###### MAIN GAME FUNCTION ENDS ######

if __name__ == "__main__":
    mainGame()
